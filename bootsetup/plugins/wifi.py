#!/usr/bin/env python3

# Copyright 2019 Sam Tygier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import subprocess

import bootsetup.plugin

wpa_supplicant_template = """ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country={country}

{net_details}
"""

@bootsetup.plugin.register("wifi")
class wifi(bootsetup.plugin.setup_module):
	def __init__(self, config):
		self.config = config
		self.wpa_supplicant_path = "/etc/wpa_supplicant/wpa_supplicant.conf"
		self.test_mode = self.config["DEFAULT"].getboolean("testmode")
		if self.test_mode:
			self.wpa_supplicant_path = "test/wpa_supplicant.conf"

	def enable(self):
		logging.info("Setting up client wifi")
		try:
			essid = self.config[self.name]['wpa_essid']
			password = self.config[self.name]['wpa_password']
			country = self.config[self.name]['wpa_country']
		except KeyError:
			logging.error(name+" section of config file must have wpa_essid, wpa_password, wpa_country defined for client mode")
			sys.exit(1)

		net_details = subprocess.check_output(["wpa_passphrase", essid, password]).decode("utf-8")

		with open(self.wpa_supplicant_path, "w") as outfile:
			outfile.write(wpa_supplicant_template.format(**locals()))
		logging.info("Wrote: %s"%self.wpa_supplicant_path)

		return True

	def disable(self):
		with open(self.wpa_supplicant_path, "w") as outfile:
			outfile.write(wpa_supplicant_template.format(country="gb", net_details=""))
		logging.info("Wrote: %s"%self.wpa_supplicant_path)
