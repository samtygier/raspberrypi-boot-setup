#!/usr/bin/env python3

# Copyright 2019 Sam Tygier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import logging
import subprocess

import bootsetup.plugin
from bootsetup.plugin import backup_file, file_flag

"""
Based on set up from:
https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md
Requires dnsmasq hostapd

"""

wpa_supplicant_template = """ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country={country}

{net_details}
"""
dhcpcd_template_on = """interface wlan0
    static ip_address=192.168.4.1/24
    nohook wpa_supplicant

"""

dhcpcd_template_off = """

"""

dnsmasq_template = """interface=wlan0      # Use the require wireless interface - usually wlan0
dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
"""
hostapd_template = """interface=wlan0
driver=nl80211
ssid={essid}
hw_mode={hostap_mode}
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase={password}
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP

"""

@bootsetup.plugin.register("wifi_hostap")
class wifi_hostap(bootsetup.plugin.setup_module):
	def __init__(self, config):
		self.config = config
		self.wpa_supplicant_path = "/etc/wpa_supplicant/wpa_supplicant.conf"
		self.dhcpcd_path = "/etc/dhcpcd.conf"
		self.dnsmasq_path = "/etc/dnsmasq.conf"
		self.hostapd_path = "/etc/hostapd/hostapd.conf"
		self.hostapd_default_path = "/etc/default/hostapd"
		self.test_mode = self.config["DEFAULT"].getboolean("testmode")
		if self.test_mode:
			self.wpa_supplicant_path = "test/wpa_supplicant.conf"
			self.dhcpcd_path = "test/dhcpcd.conf"
			self.dnsmasq_path = "test/dnsmasq.conf"
			self.hostapd_path = "test/hostapd.conf"
			self.hostapd_default_path = "test/default_hostapd"

	def enable(self):
		logging.info("Setting up wifi-hostap")
		logging.info("Temporarily disable dnsmasq and hostapd")

		if not self.test_mode:
			subprocess.check_call(["systemctl", "stop", "wpa_supplicant"])

		try:
			essid = self.config[self.name]['hostap_essid']
			password = self.config[self.name]['hostap_password']
			hostap_mode = self.config[self.name]['hostap_mode']
		except KeyError:
			logging.error(name+" section of config file must have wpa_essid, wpa_password, wpa_country defined for client mode")
			sys.exit(1)

		backup_file(self.dhcpcd_path)
		with open(self.dhcpcd_path, "w") as dhcpcd_file:
			dhcpcd_file.write(file_flag + "\n")
			dhcpcd_file.write(dhcpcd_template_on)
		logging.info("Wrote: %s"%self.dhcpcd_path)

		backup_file(self.dnsmasq_path)
		with open(self.dnsmasq_path, "w") as dnsmasq_file:
			dnsmasq_file.write(file_flag + "\n")
			dnsmasq_file.write(dnsmasq_template)
		logging.info("Wrote: %s"%self.dnsmasq_path)

		if not self.test_mode:
			subprocess.check_call(["systemctl", "--no-block", "restart", "dnsmasq"])

		backup_file(self.hostapd_path)
		with open(self.hostapd_path, "w") as hostapd_file:
			hostapd_file.write(file_flag + "\n")
			values = dict(essid=essid, password=password, hostap_mode=hostap_mode)
			hostapd_file.write(hostapd_template.format(**values))
		logging.info("Wrote: %s"%self.hostapd_path)

		if not self.test_mode:
			ret = subprocess.call(["systemctl", "--no-block", "restart", "hostapd"])
			if ret != 0:
				logging.info("Failed to start hostapd")
				return False
		return True

	def disable(self):
		backup_file(self.dhcpcd_path)
		with open(self.dhcpcd_path, "w") as dhcpcd_file:
			dhcpcd_file.write(file_flag + "\n")
			dhcpcd_file.write(dhcpcd_template_off)
		logging.info("Wrote: %s"%self.dhcpcd_path)
		
		logging.info("Disabling dnsmask and hostapd")
		if not self.test_mode:
			subprocess.check_call(["systemctl", "disable", "--now", "dnsmasq"])
			subprocess.check_call(["systemctl", "disable", "--now", "hostapd"])
