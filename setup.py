#!/usr/bin/env python3

from setuptools import setup
from setuptools.command.install import install
import os

class InstallExtra(install):
	def run(self):
		install.run(self)

		# copy systemd unit and set correct path
		curdir = os.path.dirname(os.path.realpath(__file__))
		with open("/lib/systemd/system/boot_setup.service", "w") as outfile:
			for line in open(os.path.join(curdir, "boot_setup.service")):
				if line.startswith("ExecStart"):
					line = "ExecStart=" + os.path.join(self.install_scripts, "boot_setup.py") + "\n"
				outfile.write(line)

setup(
	name="BootSetup",
	version="0.1",
	packages=["bootsetup", "bootsetup/plugins"],
	scripts=['boot_setup.py'],
	author="Sam Tygier",
	author_email="samtygier@yahoo.co.uk",
	description="A tool to allow quick configuration of a Raspberry Pi through a single file on the boot partition",
	keywords="Raspberry Pi, configuration",
	url="https://gitlab.com/samtygier/raspberrypi-boot-setup",
	classifiers=[
		'Programming Language :: Python :: 3',
		'Operating System :: POSIX :: Linux',
		'License :: OSI Approved :: GNU Affero General Public License v3',
		'Topic :: Software Development :: Embedded Systems',
	],
	python_requires='>=3.7',
	cmdclass={'install': InstallExtra}
)
