#!/usr/bin/env python3

# Copyright 2019 Sam Tygier
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import configparser
import logging
import subprocess
import bootsetup.plugin

testmode = "test" in sys.argv

config_path = "/boot/boot_setup.conf"
if testmode:
	config_path = "test/boot_setup.conf"


def is_enabled(config, name):
	"Enabled if it exist and does not have enable=false"
	if name in config:
		if "enable" in config[name]:
			# have "enable" entry
			if not config[name].getboolean("enable"):
				return False
			else:
				return True
		else:
			# default enable
			return True
	else:
		# No entry in file
		return False

def main():
	plugins = bootsetup.plugin.load_plugins([])

	if not os.path.exists(config_path):
		make_empty_config()
	
	config = configparser.ConfigParser()
	config.read(config_path)
	config["DEFAULT"]["testmode"] = "true" if testmode else "false"

	all_conf_sections = set(config.keys())
	all_conf_sections.remove("DEFAULT")

	for plugin_name, pg in plugins.items():
		if plugin_name in all_conf_sections:
			all_conf_sections.remove(plugin_name)
		plugin = pg(config)
		if is_enabled(config, plugin_name):
			logging.info("Section enabled: "+plugin_name)
			plugin.enable()
		else:
			logging.info("Section disabled: "+plugin_name)
			plugin.disable()

	for conf_sect in all_conf_sections:
		logging.warning("Unused section in config: "+conf_sect)

	return True


empty_config = """
# [wifi]
# wpa_essid = network_name
# wpa_password = foobarbaz
# wpa_country = gb
"""

def make_empty_config():
	logging.warning("No config file. Creating "+config_path)
	with open(config_path, "w") as outfile:
		outfile.write(empty_config)

if __name__ == "__main__":
	logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')
	logging.info("Starting")
	if main():
		logging.info("Finished")
	else:
		logging.error("Finished with errors")
